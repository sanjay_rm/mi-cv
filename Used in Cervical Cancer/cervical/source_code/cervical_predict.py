# Required modules
import pandas as pd
import json
import numpy as np
import matplotlib.pyplot as plt
from catboost import CatBoostClassifier
from sklearn import metrics
import seaborn as sns
from cross_validation import CrossValidation
from model_interpretation import ModelInterpretation

def predicts(cb_model):
    # Loading the fit model, and the out-of-sample test files

    main_obj = CrossValidation()
    main_obj.cross_validation_classifier_predict(cb_model, data_dir, graph_dir, 'Biopsy')


def plots(cb_model,df,cat_f):
    sec_obj=ModelInterpretation()
    # FEATURE IMPORTANCE PLOT
    sec_obj.feature_importance(df,cb_model,graph_dir,'Biopsy')
    # SHAP PLOT
    unseen_x_test = pd.read_csv("unseen_x_test.csv")
    unseen_y_test = pd.read_csv("unseen_y_test.csv", header=None)
    sec_obj.shap_vals(cb_model,unseen_x_test,unseen_y_test,graph_dir, cat_f)
    # INCLUDE PLOT CALLS OF WHATEVER COLUMNS YOU WANT TO PLOT AGAINST EACH OTHER
if __name__ == "__main__":

    # Reading required directories from json file
    jsonfile = r"/home/sanjay/Desktop/cervical/json/cervical_schema.json"
    json_file = open(jsonfile)
    config = json.load(json_file)
    data_dir = config["datadir"]
    model_dir = config["modeldir"]
    graph_dir = config["graphdir"]
    df = pd.read_csv(data_dir + "6.csv")
    # SPECIFY ALL THE CATEGORICAL FEATURES INDICES BELOW (COLUMN NUMBERS)
    cat_f=[]
    # Calling the predict and plot functions
    from_file = CatBoostClassifier()
    cb_model = from_file.load_model("catboost_classifier_model", format="cbm")
    predicts(cb_model)
    plots(cb_model,df,cat_f)
