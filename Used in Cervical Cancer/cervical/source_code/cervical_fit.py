import pandas as pd
from sklearn.model_selection import train_test_split
import json
from catboost import CatBoostClassifier
from cross_validation import CrossValidation

class CatboostClassifierCervicalPrediction:
    def __init__(self):
        pass

    def initialize_dataframe(self):
        # Importing the data frame and performing minor modifiications, followed by splitting into train and test sets
        df = pd.read_csv(data_dir+"6.csv")
        list_values = df.columns.values.tolist()
        list_values.remove('Biopsy')
        x = df[list_values]
        y = df['Biopsy']
        x_train, x_test, y_train, y_test = train_test_split(x, y, test_size=0.2, random_state=23, shuffle=False)
        return x, y, x_train, x_test, y_train, y_test

    def initialize_classifier(self):
        # Initializing CatBoostRegressor with observed best parameters
        cb_model = CatBoostClassifier(iterations=500, learning_rate=0.01, max_depth=5)
        return cb_model

    def fits(self, x, y, x_train, x_test, y_train, y_test, cb_model,data_dir,model_dir):
        main_obj = CrossValidation()
        main_obj.cross_validation_classifier_fit(x,y,cb_model,data_dir,model_dir)


if __name__ == "__main__":

    # Reading required directories from json file
    jsonfile = r"/home/sanjay/Desktop/cervical/json/cervical_schema.json"
    json_file = open(jsonfile)
    config = json.load(json_file)
    data_dir = config["datadir"]
    model_dir = config["modeldir"]
    graph_dir = config["graphdir"]


    obj = CatboostClassifierCervicalPrediction()  
    X, y, X_train, X_test, y_train, y_test = obj.initialize_dataframe()
    cb_model = obj.initialize_classifier()
    obj.fits(X, y, X_train, X_test, y_train, y_test, cb_model,data_dir,model_dir)

