import numpy as np
from sklearn import metrics
import matplotlib.pyplot as plt
import seaborn as sns
import shap
from catboost import Pool


class ModelInterpretation:
    def __init__(self):
        pass

    def roc_auc_score_and_plot(self, actual, predicted, graph_dir, target_variable):
        scr = metrics.roc_auc_score(actual, predicted)
        print("AUC score is: ", scr)
        x = [i for i in range(1, len(actual) + 1)]
        errors = []
        plt.style.use('dark_background')
        for i in range(1, len(predicted) + 1):
            if predicted[i - 1] - actual[i - 1] != 0:
                errors.append(i)
        plt.scatter(x, actual, label='Actual value of ' + str(target_variable), s=300)
        plt.scatter(x, predicted, color='red', label='Predicted value of ' + str(target_variable))
        plt.bar(errors, [1 for i in range(0, len(errors))], width=1.5, color='brown')
        plt.xlabel("Instance number")
        plt.ylabel(target_variable)
        plt.legend(loc='upper left')
        plt.savefig(graph_dir+str(target_variable) + "_roc_auc_graph.png")
        plt.show()

    def f1_score_and_plot(self, actual, predicted, graph_dir, target_variable):
        scr = metrics.f1_score(actual, predicted)
        print("AUC score is: ", scr)
        x = [i for i in range(1, len(actual) + 1)]
        errors = []
        plt.style.use('dark_background')
        for i in range(1, len(predicted) + 1):
            if predicted[i - 1] - actual[i - 1] != 0:
                errors.append(i)
        plt.scatter(x, actual, label='Actual value of ' + str(target_variable), s=300)
        plt.scatter(x, predicted, color='red', label='Predicted value of ' + str(target_variable))
        plt.bar(errors, [1 for i in range(0, len(errors))], width=1.5, color='brown')
        plt.xlabel("Instance number")
        plt.ylabel(target_variable)
        plt.legend(loc='upper left')
        plt.savefig(graph_dir+str(target_variable) + "_f1_graph.png")
        plt.show()

    def rmse_value_and_plot(self, actual, predicted, graph_dir, target_variable):
        rmse_value = np.sqrt(metrics.mean_squared_error(actual, predicted))
        print("RMSE value is: ", rmse_value)
        x = [i for i in range(1, len(actual) + 1)]
        plt.plot(x, actual, label='Actual value of ' + str(target_variable))
        plt.plot(x, predicted, color='red', label='Predicted value of ' + str(target_variable))
        plt.xlabel("Instance number")
        plt.ylabel(target_variable)
        plt.legend(loc='upper left')
        plt.savefig(graph_dir+str(target_variable) + "_rmse_graph.png")
        plt.show()

    def r2_value_and_plot(self, actual, predicted, graph_dir, target_variable):
        r2_value = np.sqrt(metrics.r2_score(actual, predicted))
        print("RMSE value is: ", r2_value)
        x = [i for i in range(1, len(actual) + 1)]
        plt.plot(x, actual, label='Actual value of ' + str(target_variable))
        plt.plot(x, predicted, color='red', label='Predicted value of ' + str(target_variable))
        plt.xlabel("Instance number")
        plt.ylabel(target_variable)
        plt.legend(loc='upper left')
        plt.savefig(graph_dir+str(target_variable) + "_r2_graph.png")
        plt.show()

    def swarm_plot(self, data, name_1, name_2):
        sns.swarmplot(x=name_1, y=name_2, data=data)
        plt.show()

    def cat_plot(self, data, name_1, name_2):
        sns.catplot(x=name_1, y=name_2, data=data)
        plt.show()

    def boxen_plot(self, data, name_1, name_2):
        sns.boxenplot(x=name_1, y=name_2, data=data)
        plt.show()

    def bar_plot(self, data, name_1, name_2):
        sns.barplot(x=name_1, y=name_2, data=data)
        plt.show()

    def feature_importance(self, df, cb_model, graph_dir, target_variable):
        list_values = df.columns.values.tolist()
        list_values.remove(target_variable)
        x = df[list_values]
        xaxis = cb_model.get_feature_importance()
        yaxis = x.columns.values.tolist()
        plt.barh(yaxis, xaxis)
        plt.xlabel("Feature Importance Score")
        plt.ylabel("Features")
        plt.savefig(graph_dir+"feature_importance.png")
        plt.show()

    def shap_vals(self, model, x_test, y_test, graph_dir, cat_f):
        shap_values = model.get_feature_importance(Pool(x_test, label=y_test, cat_features=cat_f), type="ShapValues")
        shap_values = shap_values[:, :-1]
        shap.summary_plot(shap_values, x_test)
        plt.savefig(graph_dir + "shap_graph.png")
        plt.show()


