import pandas as pd
from sklearn.model_selection import KFold
import numpy as np
import matplotlib.pyplot as plt
from sklearn import metrics
# May need to remove first column of unseen as it is autonatically numbers

class CrossValidation:
    def __init__(self):
        pass

    def cross_validation_regressor_fit(self, x, y, cb_model, data_dir, model_dir, num_splits=5):
        scores = []
        kf = KFold(n_splits=num_splits, random_state=None)
        i = 0
        for result in kf.split(x):
            i += 1
            x_train = x.iloc[result[0]]
            y_train = y.iloc[result[0]]
            x_test = x.iloc[result[1]]
            y_test = y.iloc[result[1]]
            # Fitting the model
            print(cb_model.get_feature_importance())
            if i == num_splits:
                x_test.to_csv(data_dir+"unseen_x_test" + ".csv")
                y_test.to_csv(data_dir+"unseen_y_test" + ".csv")
            if i != num_splits:
                model = cb_model.fit(x_train, y_train, use_best_model=True, verbose=True)
            # Predicting on the test set using the already fit model
            predictions = model.predict(x_test)
            rmse_value = np.sqrt(metrics.mean_squared_error(y_test, predictions))
            scores.append(rmse_value)
        print("Average RMSE Cross-validation value: ", sum(scores) / (num_splits - 1))
        cb_model.save_model(model_dir+"catboost_regressor_model", format="cbm")

    def cross_validation_regressor_predict(self,cb_model, data_dir, graph_dir, target_variable):
        unseen_x_test = pd.read_csv(data_dir+"unseen_x_test.csv")
        unseen_y_test = pd.read_csv(data_dir+"unseen_y_test.csv", header=None)
        predictions = cb_model.predict(unseen_x_test)
        new_y_test = unseen_y_test[unseen_y_test.columns[1]]
        rmse_value = np.sqrt(metrics.mean_squared_error(new_y_test, predictions))
        print("RMSE: ", rmse_value)
        x = [i for i in range(1, len(unseen_y_test) + 1)]
        plt.plot(x, new_y_test, label='Actual no. of ' + str(target_variable))
        plt.plot(x, predictions, color='red', label='Predicted no. of ' + str(target_variable))
        plt.xlabel("Instance number")
        plt.ylabel(target_variable)
        plt.legend(loc='upper left')
        plt.savefig(graph_dir+"Main_graph_regressor.png")
        plt.show()

    def cross_validation_classifier_fit(self, x, y, cb_model, data_dir, model_dir, num_splits=5):
        scores = []
        kf = KFold(n_splits=num_splits, random_state=None)
        i = 0
        for result in kf.split(x):
            i += 1
            x_train = x.iloc[result[0]]
            y_train = y.iloc[result[0]]
            x_test = x.iloc[result[1]]
            y_test = y.iloc[result[1]]
            # Fitting the model
            #print(cb_model.get_feature_importance())
            if i == num_splits:
                x_test.to_csv(data_dir+"unseen_x_test" + ".csv")
                y_test.to_csv(data_dir+"unseen_y_test" + ".csv")
            if i != num_splits:
                model = cb_model.fit(x_train, y_train, use_best_model=True, verbose=True)
            # Predicting on the test set using the already fit model
            y_pred_class = cb_model.predict(x_test)
            y_pred_prob = cb_model.predict_proba(x_test)[:, 1]
            new_y_test = y_test[y_test.columns[1]]
            fpr, tpr, thresholds = metrics.roc_curve(new_y_test, y_pred_class)
            scr = metrics.roc_auc_score(new_y_test, y_pred_class)
            scores.append(scr)
        print("Average ROC-AUC Cross-validation score: ", sum(scores) / 5)
        cb_model.save_model(model_dir+"catboost_classifier_model", format="cbm")

    def cross_validation_classifier_predict(self,cb_model, data_dir, graph_dir, target_variable):
        # Predicting on the test set using the already fit model

        unseen_x_test = pd.read_csv(data_dir+"unseen_x_test.csv")
        unseen_y_test = pd.read_csv(data_dir+"unseen_y_test.csv", header=None)
        y_pred_class = cb_model.predict(unseen_x_test)
        y_pred_prob = cb_model.predict_proba(unseen_x_test)[:, 1]
        new_y_test = unseen_y_test[unseen_y_test.columns[1]]
        fpr, tpr, thresholds = metrics.roc_curve(new_y_test, y_pred_class)
        scr = metrics.roc_auc_score(new_y_test, y_pred_class)
        print("ROC-AUC score on unseen set", scr)
        x = [i for i in range(1, len(new_y_test) + 1)]
        errors = []
        plt.style.use('dark_background')
        for i in range(1, len(y_pred_class) + 1):
            if y_pred_class[i - 1] - new_y_test[i - 1] != 0:
                errors.append(i)
        plt.scatter(x, new_y_test, label='Actual value of ' + str(target_variable), s=300)
        plt.scatter(x, y_pred_class, color='red', label='Predicted value of ' + str(target_variable))
        plt.bar(errors, [1 for i in range(0, len(errors))], width=1.5, color='brown')
        plt.xlabel("Instance number")
        plt.ylabel(target_variable)
        plt.legend(loc='upper left')
        plt.savefig(graph_dir+"Main_graph_classifier.png")
        plt.show()


